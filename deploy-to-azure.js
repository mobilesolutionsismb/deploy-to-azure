#!/usr/bin/env node
const path = require('path');
const util = require('util');
const fs = require('fs-extra');
const program = require('commander');
const colors = require('colors/safe');
const aps = require('azure-publish-settings');
const kuduApi = require('kudu-api');
const FolderZip = require('folder-zip');

const pkg = require('./package.json');

let PUBLISH_SETTINGS = null; // 'ireactfe(testsftp).PublishSettings';
let DIRNAME = null;
const DEFAULT_DIR = 'dist';
const DEFAULT_OUTPUT_FOLDER = '/site/wwwroot/';

program
    .version(pkg.version)
    .usage('[options] <PublishSettings file>')
    .option('-v, --verbose', 'Verbose mode')
    .option('-p, --publish-settings <path-to-PublishSettings>', '<Required> path to publish settings file')
    .option('-d, --dir [path-to-directory]', '[Optional] path to the folder to be zipped (default to dist)')
    .option('-o, --output-folder [path-to-remote-output-folder]', '[Optional] path to the remote destination folder on Azure WebApp (default to /site/wwwroot/)')
    .parse(process.argv);

const config = {
    verbose: program.verbose === true,
    publishSettings : program.publishSettings,
    dir: program.dir || DEFAULT_DIR,
    outputFolder: program.outputFolder || DEFAULT_OUTPUT_FOLDER
};

const simpleTypeRx = /string|number|boolean/i;

function _simplify() {
    return [...arguments].map((a) => {
        let type = typeof a;
        return simpleTypeRx.test(type) ? a : util.inspect(a);
    });
}

function promptError() {
    let args = _simplify.apply(null, arguments);
    console.error(colors.red.apply(null, args));
}

function promptSuccess() {
    let args = _simplify.apply(null, arguments);
    console.log(colors.green.apply(null, args));
}


function promptInfo() {
    let args = _simplify.apply(null, arguments);
    console.log(colors.cyan.apply(null, args));
}

function promptVerbose() {
    if (config.verbose === true) {
        let args = _simplify.apply(null, arguments);
        console.log(colors.magenta.apply(null, args));
    }
}


function deploy() {
    aps.readAsync(config.publishSettings)
        .then(settings => {
            // Use the "kudu" property to pass credentials directly to kudu-api
            let { url, name } = settings;
            const kudu = kuduApi(settings.kudu);
            const zip = new FolderZip();
            const ZIP_FILE = `${DIRNAME}.zip`;

            zip.zipFolder(DIRNAME, { excludeParentFolder: true }, () => {
                promptVerbose(`Archiving ${config.dir} to ${ZIP_FILE}...`);
                zip.writeToFile(ZIP_FILE, err => {
                    if (err) {
                        promptError(err);
                    }
                    promptVerbose(`Deploying ${config.dir} to ${name} in ${config.outputFolder}...`);

                    kudu.zip.upload(ZIP_FILE, config.outputFolder, err => {
                        if (err) {
                            promptError('Error uploading the directory', err);
                        }
                        else {
                            promptSuccess(`Folder "${config.dir}" uploaded successfully! Open it at ${url}.`);
                        }
                        //CLEAN
                        fs.remove(ZIP_FILE, err => {
                            if (err) {
                                promptError('Error cleaning the archive file', err);
                            }
                        });
                    });
                });
            });

        }).catch(err => {
            promptError(err);
        });
}

if (process.argv.length >= 2) {
    if (!program.publishSettings) {
        console.log('Missing required argument --publish-settings');
        program.help();
    }
    else {
        PUBLISH_SETTINGS = path.resolve('.', config.publishSettings);
        DIRNAME = path.resolve('.', config.dir);
        promptVerbose('Reading publish settings from', PUBLISH_SETTINGS);
        promptVerbose('Input directory', DIRNAME);
        promptVerbose('Remote destination directory', DEFAULT_OUTPUT_FOLDER);
        fs.stat(PUBLISH_SETTINGS, (err, stat) => {
            if (err) {
                promptError('Fatal Error:', err.message);
                process.exit(1);
            }
            if (stat.isFile()) {
                fs.stat(DIRNAME, (err, stat) => {
                    if (err) {
                        promptError('Fatal Error:', err.message);
                        process.exit(1);
                    }
                    if (stat.isDirectory()) {
                        deploy();
                    } else {
                        promptError(`Error: ${DIRNAME} is not a directory`);
                    }
                });
            } else {
                promptError(`Error: ${PUBLISH_SETTINGS} is not a file`);
            }
        });
    }
}
else {
    program.help();
}