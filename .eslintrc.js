module.exports = {
  "env": {
    "es6": true,
    "node": true
  },
  // "parser": "babel-eslint",
  "extends": "eslint:recommended",
  "installedESLint": true,
  "parserOptions": {
    "ecmaFeatures": {
      "experimentalObjectRestSpread": true
    },
    "sourceType": "module"
  },
  "globals": {
  },
  "rules": {
    "no-unused-vars": "off",
    "no-console": "off",
    "no-fallthrough": "off",
    "no-inner-declarations": "off",
    "indent": [
      "off",
      2
    ],
    "linebreak-style": [
      "off"
    ],
    "quotes": [
      "error",
      "single",
      {
        "avoidEscape": true,
        "allowTemplateLiterals": true
      }
    ],
    "semi": [
      "error",
      "always"
    ]
  }
};