# DEPLOY FOLDER TO AZURE

This utility helps deploying static contents to azure, using Kudu api.  
Typical use case: you have to upload a SPA to Azure WebApp using PublishSettings.

## Usage

`node deploy-to-azure -p <YOUR .PublishSettings file> [-d <dist folder>][-o <destination folder on Azure>]`

### Synopsis

```bash
  Usage: deploy-to-azure [options] <PublishSettings file>

  Options:

    -h, --help                                          output usage information
    -V, --version                                       output the version number
    -v, --verbose                                       Verbose mode
    -p, --publish-settings <path-to-PublishSettings>    <Required> path to publish settings file
    -d, --dir [path-to-directory]                       [Optional] path to the folder to be zipped (default to dist)
    -o, --output-folder [path-to-remote-output-folder]  [Optional] path to the remote destination folder on Azure WebApp (default to /site/wwwroot/)
```

## Technical notes

This package relies on https://github.com/itsananderson/kudu-api